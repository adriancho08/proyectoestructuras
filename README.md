# Como usar git (conceptos basicos)

[![N|Solid](https://upload.wikimedia.org/wikipedia/commons/thumb/e/e0/Git-logo.svg/125px-Git-logo.svg.png)](https://nodesource.com/products/nsolid)

Se inicia un repositorio en git.

```sh
git init
```

Si ya existe un repositorio y se desea trabajar sobre el entonces utilizamos.

```sh
git clone username@host:/path/to/repository
```

Cuando agregamos nuevos archivos es necesario agregarlos al seguimiento del historial de cambios

```sh
// Agregar todos los archivos
git add .

//Solo agregar el archivo que deseo agregar a mi repositorio
git add <filename>
```

Por el contario si tu archivo ya esta siendo rastreado pero realizaste cambios en el tienes que registrar que es lo que hiciste por medio de este comando

```
git commit -am "Commit message"
```

#### Solo el usuario principal deberia mezclar los codigos por eso es necesario lo siguiente.

Se debe crear una nueva rama basandonos en la rama principal "master" para el manejo del codigo propio

```
git checkout -b feature_x
```

despues de hacer todo esto se puede subir nuestro codigo con los cambios realizados

```
git push origin <branch>
// o si estamos en una rama personal
git push origin <feature_x>
```

#### Finalmente siempre que vamos a iniciar a trabajar es necesario traer los nuevos cambios hechos en la rama principal para asi estar actualizados.

este comando nos sirve si deseamos traernos todas las ramas creadas en el servidor

```
git fetch
```

y este nos sirve si deseamos traer a nuestra rama las actualizaciones del servidor

```
git pull origin <branch>
```

Pero la manera correcta de hacerlo es primero traer los cambios a cada rama y luego mezclarla con la que estamos trabajando

```
git merge <branch>
```


### Otras funcionalidades utiles

Saber el estado actual de mi proyecto y mis cambios

```
git status
```

Ver los cambios que se han hecho en el proyecto incluso los comentarios que otros colaboradores han escrito

```
git log
```