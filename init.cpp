#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ncurses.h>
#include <time.h>

using namespace std;

struct persona;

typedef struct tiquete {
    int tipo;
    char *codigo;
    char *clase;
    char *fecha_salida;
    char *hora_salida;
    char *fecha_llegada;
    char *hora_llegada;
    int silla;
    persona *asigancion;
    struct tiquete *sig;
    struct tiquete *ant;
}vuelo;
 
typedef struct persona{
    int documento;
    char *nombre;
    char *apellido;
    char *telefono;
    char *fecha_nacimiento;
    char genero;
    vuelo *tiket;
    struct persona *sig;
    struct persona *ant;
}pasajero;

typedef pasajero *tpasajero;
typedef vuelo *tvuelo;

void clearScreen (){
    system("clear");
    //system("cls");
}

void menu() {
    cout << "//////////////////////  GOLONDRINA VELOZ  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\" << endl;
    cout << "//////////////////////////  TIQUETES  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\" << endl;
    cout << "  1. Comprar Tiquete            |  5. Cambiar Silla             " << endl;
    cout << "  2. Modificar Pasajero         |  6. Imprimir pase de abordar  " << endl;
    cout << "  3. Listar Pasajeros           |  7. Cancelar Tiquete          " << endl;
    cout << "  4. Buscar pasajero            |  8. Salir                     " << endl;
    cout << "\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ ///////////////////////////////" << endl;
}

void insertarPasajero(tpasajero *cpasajero, tvuelo *cvuelo){
    int dia, mes, year;
    
    tpasajero nuevo_pasajero;
    nuevo_pasajero = (pasajero*)malloc(sizeof(persona));
    
    cout << "Por favor ingrese los datos necesarios para registrar el pasajero \nNombres: ";
    cin >> nuevo_pasajero->nombre;
    
    cout << "Apellidos: ";
    cin >> nuevo_pasajero->apellido;
    
    cout << "Telefono - Celular: ";
    cin >> nuevo_pasajero->telefono;
    
    cout << "Fecha de nacimiento (dia/mes/año): ";
    cin >> dia, mes, year;
    
    nuevo_pasajero->tiket = *cvuelo;
    nuevo_pasajero->sig = *cpasajero;
    
    *cpasajero = nuevo_pasajero;
}

void imprimePasajeros(tpasajero cabeza){
    while (cabeza != NULL)
    {
       cout << "Nombre completo: "<< cabeza->nombre << cabeza->apellido;
       cout << "Nombre completo: "<< cabeza->nombre << cabeza->apellido << endl;
       cabeza = cabeza->sig;
    }
  
}

void comprarTiquete () {
    cout << "///  Comprar Tiquete  ///" << endl;
    cout << "Tipo vuelo: ";
    cin.get();
    cout << "Código vuelo: ";
    cin.get();
    cout << "Documento pasajero: ";
    cin.get();
    cout << "Nombre pasajero: ";
    cin.get();
    cout << "Apellido pasajero: ";
    cin.get();
    cout << "Teléfono pasajero: ";
    cin.get();
    cout << "Fecha de nacimiento: ";
    cin.get();
    cout << "Genero: ";
    cin.get();
    cout << "Clase de tiquete: ";
    cin.get();
    cout << "Fecha vuelo: ";
    cin.get();
    cout << "Hora salida: ";
    cin.get();
    cout << "Fecha llegada: ";
    cin.get();
    cout << "Hora llegada ";
    cin.get();
    cout << "Silla: ";
    cin.get();
}

int calc_edad(int dia, int mes, int anio){

    time_t tiempo;
    struct tm *fecha_local;
    tiempo = time(NULL);
    fecha_local = localtime(&tiempo);
    
    int dia_actual = fecha_local->tm_mday;
    int mes_actual = fecha_local->tm_mon+1;
    int anio_actual = fecha_local->tm_year+1900;

    if (dia > 0 && dia <= 31 && mes > 0 && mes <= 12 && anio > 1900 && anio <= anio_actual){

        int dias_mes[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        int dias, meses, anios;
    
        if (mes == 2){
            if (dia <= 29){
                if ((anio_actual % 4 == 0 && anio_actual % 100 != 0) || (anio_actual % 400 == 0)){
                    dias_mes[mes-1]=29;
                }
            }else{
                return 0; // Fecha no valida
            }
        }else if (mes == 4 || mes == 6 || mes == 9 || mes == 11){
            if (dia > 30){
                return 0; // Fecha no valida
            }
        }
    
        if (dia > dia_actual){
            dia_actual += dias_mes[mes-2]; // + dias del mes anterior al actual
            mes_actual -= 1;
        }
    
        if (mes > mes_actual){
            mes_actual += 12;
            anio_actual -= 1;
        }
    
        // dias = dia_actual - dia;
        // meses = mes_actual - mes;
        anios = anio_actual - anio;
      
        //cout << "Anios: " << anios << " Meses: " << meses  << " Dias: " << dias << endl;

        return anios;
    }
    return 0; //Fecha no valida
}

void modificarPasajero () {
    cout << "///  Modificar Pasajero  ///" << endl;
}

void listarPasajeros () {
    cout << "///  Listar Pasajeros  ///" << endl;
}

void buscarPasajero () {
    cout << "///  Buscar Pasajero  ///" << endl;
}

void cambiarSilla () {
    cout << "///  Cambiar Silla  ///" << endl;
}

void imprimirPaseAbordar () {
    cout << "///  Imprimir Pase de Abordar  ///" << endl; 
}

void cancelarTiquete () {
    cout << "///  Cancelar Tiquete  ///" << endl;
}

int main()
{
    int opt=0;
    while (opt != 8){
        clearScreen();
        menu();
        cout << "Option: ";
        cin >> opt;
        switch (opt){
            case 1:
                comprarTiquete ();
                break;
            case 2:
                modificarPasajero ();
                break;
            case 3:
                listarPasajeros ();
                break;
            case 4:
                buscarPasajero ();
                break;
            case 5:
                cambiarSilla ();
                break;
            case 6:
                imprimirPaseAbordar ();
                break;
            case 7:
                cancelarTiquete ();
                break;
            case 8:
                cout << "/////////////////////////  BUEN VIAJE  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\" << endl;
                break;
            // default :
            //   cout << "Opcion incorrecta!." << endl;
            //   break; 
        }
        cout << "Press (enter) para continuar.";
        getchar();
        getchar();
    }

    return 0;
}
